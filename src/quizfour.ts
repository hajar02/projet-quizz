const question = document.querySelector<HTMLBodyElement>('.question');
const btnA = document.querySelector<HTMLButtonElement>('.reponseone');
const btnB = document.querySelector<HTMLButtonElement>('.reponsetwo');
const btnC = document.querySelector<HTMLButtonElement>('.reponsethree');
const btnD = document.querySelector<HTMLButtonElement>('.reponsefour');
const timer: any = document.querySelector<HTMLElement>('.circle')  //timer
const comment:any = document.querySelector<HTMLElement>('#comment');
const score: any = document.querySelector<HTMLElement>('#score');
const lance: any = document.querySelector<HTMLButtonElement>('#lance')
let qcm: string[] = [
    "Question 1?",
    "Question2?",
    "Question3?",
    "Question4?",
    "Question5?",
    "Question6?",
    "Question7?",
    "Question8?",
    "Question9?",
    "Question10?",
];
let answers: string[][] = [
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"],
    ["Reponse 1", "Reponse 2", "Reponse 3", "Reponse 4"]
];
let boutonAnswers: any[] = [btnD, btnA, btnC, btnC, btnD, btnB, btnA, btnC, btnC, btnD];
let counterQcm = 0;

let pointScore:number = 0;

let i = 10;

const debut: any = document.querySelector<HTMLElement>('#start');
const quiz: any = document.querySelector<HTMLElement>('#principal');
const final: any = document.querySelector<HTMLElement>('#final');

questionSuivante();
reponseSuivante()

lance.addEventListener('click', () => {
    quiz.style.display = 'block';
    debut.style.display = 'none';
    final.style.display = 'none';



    setInterval(function () {

        if (i == -1) {
            counterQcm++;
            questionSuivante();
            reponseSuivante();
            i = 10;
        }
        timer.textContent = i--;

    }, 1000);
});

function loadQuiz() {
    counterQcm++;
    questionSuivante();
    reponseSuivante();
    i = 10;

}


btnA?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnA) {
        //true
        if (timer) {
            timer.style.backgroundColor = 'rgba(0, 128, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
            pointScore++;
            score.innerHTML = pointScore ;
            console.log(pointScore);
            
        }
    } else {
        //false
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 0, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
        }
    }

    setTimeout(() => {
        console.log('timed')
        questionSuivante();
        reponseSuivante();
        loadQuiz();
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 255, 255)'
            timer.style.color = 'rgba(0, 0, 0)'
        }
    }, 900)

    if (pointScore >= 7){
        comment.innerHTML = "Comment :("
    };
    if(pointScore>=4 && pointScore<7){
        comment.innerHTML = "Comment :|"
    };
    if(pointScore <4 || pointScore==0){
        comment.innerHTML = "Comment :)"
    };
})


btnB?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnB) {
        //true
        if (timer) {
            timer.style.backgroundColor = 'rgba(0, 128, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
            pointScore++;
            score.innerHTML = pointScore;
            console.log(pointScore);

        }
    } else {

        //false
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 0, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
        }
    }

    setTimeout(() => {
        console.log('timed')
        questionSuivante();
        reponseSuivante();
        loadQuiz();
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 255, 255)'
            timer.style.color = 'rgba(0, 0, 0)'
        }
    }, 900)
    if (pointScore >= 7){
        comment.innerHTML = "Comment :("
    };
    if(pointScore>=4 && pointScore<7){
        comment.innerHTML = "Comment :|"
    };
    if(pointScore <4 || pointScore==0){
        comment.innerHTML = "Comment :)"
    };

})


btnC?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnC) {
        //true
        if (timer) {
            timer.style.backgroundColor = 'rgba(0, 128, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
            pointScore++;
            score.innerHTML = pointScore;
            console.log(pointScore);

        }
    } else {

        //false
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 0, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
        }
    }

    setTimeout(() => {
        console.log('timed')
        questionSuivante();
        reponseSuivante();
        loadQuiz();
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 255, 255)'
            timer.style.color = 'rgba(0, 0, 0)'
        }
    }, 900)
    if (pointScore >= 7){
        comment.innerHTML = "Comment :("
    };
    if(pointScore>=4 && pointScore<7){
        comment.innerHTML = "Comment :|"
    };
    if(pointScore <4 || pointScore==0){
        comment.innerHTML = "Comment :)"
    };
})

btnD?.addEventListener('click', () => {
    if (boutonAnswers[counterQcm] == btnD) {
        //true
        if (timer) {
            timer.style.backgroundColor = 'rgba(0, 128, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
            pointScore++;
            score.innerHTML = pointScore + "/10";
            console.log(pointScore);

        }
    } else {

        //false
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 0, 0)'
            timer.style.color = 'rgba(255, 255, 255)'
        }
    }

    setTimeout(() => {
        console.log('timed')
        questionSuivante();
        reponseSuivante();
        loadQuiz();
        if (timer) {
            timer.style.backgroundColor = 'rgba(255, 255, 255)'
            timer.style.color = 'rgba(0, 0, 0)'
        }
    }, 900)
    if (pointScore >= 7){
        comment.innerHTML = "Comment :("
    };
    if(pointScore>=4 && pointScore<7){
        comment.innerHTML = "Comment :|"
    };
    if(pointScore <4 || pointScore==0){
        comment.innerHTML = "Comment :)"
    };
})


function questionSuivante() {
    if (question && counterQcm < 10) {
        question.innerHTML = qcm[counterQcm];
    }
    else {
        quiz.style.display = 'none';
        debut.style.display = 'none';
        final.style.display = 'block';
    }

}

function reponseSuivante() {
    if (btnA && btnB && btnC && btnD && counterQcm < 10) {
        btnA.innerHTML = answers[counterQcm][0];
        btnB.innerHTML = answers[counterQcm][1];
        btnC.innerHTML = answers[counterQcm][2];
        btnD.innerHTML = answers[counterQcm][3];
    }
    else {
        quiz.style.display = 'none';
        debut.style.display = 'none';
        final.style.display = 'block';
    }

}







